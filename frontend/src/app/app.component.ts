import {Component, OnInit} from '@angular/core';
import {ApiService} from "./shared/services/api.service";
import {DataService} from "./data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements  OnInit{

  constructor (
      private apiService: ApiService,
      public dataService: DataService,
  ) {}

  ngOnInit(): void {
    this.apiService.getAllCats().subscribe(
        console.log,
    )
  }

}
