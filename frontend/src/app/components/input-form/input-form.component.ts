import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DataService} from "../../data.service";
import {MatDialog, MatDialogConfig} from "@angular/material";
import {ResChartsDialogComponent} from "../res-charts-dialog/res-charts-dialog.component";

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.sass']
})
export class InputFormComponent implements OnInit {

  signalForm: FormGroup;


  constructor(
      private fb: FormBuilder,
      private dialog: MatDialog,
      public dataService: DataService,
  ) { }

  ngOnInit() {
    this.createForm();
    this.signalForm.updateValueAndValidity();
    this.signalForm.markAsTouched();
  }

  clearForm(): void {
    this.signalForm.reset();
  }

  modeling(): void {
    this.dataService.modeling(this.signalForm.value);
  }

  buildCharts(): void {
    console.log('building charts...');
    const dialogConfig: MatDialogConfig = {
      width: '1000px',
      height: '600px',
      data: this.dataService.dataToBuildCharts,
    };
    const dialogRef = this.dialog.open(ResChartsDialogComponent, dialogConfig);
    dialogRef.afterClosed();
  }

  // Створення форми для введення початкових даних
  private createForm(): void {
    this.signalForm = this.fb.group({
      timeStart: [
        0,
        Validators.required,
      ],
      timeEnd: [
        1,
        Validators.required,
      ],
      gasDepth: [
        450,
        Validators.required,

      ],
      seaDepth: [
        500,
        Validators.required,

      ],
      amplitude: [
        0.002,
        Validators.required,
      ],
      frequency: [
        1024,
        Validators.required,
      ],
      soundSpeed: [
        1500,
        Validators.required,
      ],
    });
  }
}
