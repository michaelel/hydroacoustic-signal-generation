import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {ArrowDataDialogComponent} from './components/arrow-data-dialog/arrow-data-dialog.component';
import {Observable} from 'rxjs';
import {filter, tap} from 'rxjs/operators';
import {DataService} from '../../data.service';
import {ArrowDialogReqInterface} from '../../shared/interfaces/arrow-dialog-req.interface';

@Component({
  selector: 'app-input-chart',
  templateUrl: './input-chart.component.html',
  styleUrls: ['./input-chart.component.sass']
})
export class InputChartComponent implements OnInit, AfterViewInit {
  @ViewChild('inputChart') inputChart: ElementRef;

  context: CanvasRenderingContext2D;

  pointForm: FormGroup;

  hoverX: number;
  hoverY: number;

  shouldCreateLine: boolean;

  constructor(
      private fb: FormBuilder,
      private dialog: MatDialog,
      public dataService: DataService,
  ) { }

  ngOnInit() {
    this.createPointForm();
  }

  ngAfterViewInit(): void {
    this.context = (<HTMLCanvasElement>this.inputChart.nativeElement).getContext('2d');

    this.createCoordinateSystem();
  }

  createPointForm(): void {
    this.pointForm = this.fb.group({
        xPoint: ['', [Validators.required, Validators.min(-15000), Validators.max(15000)]],
        yPoint: ['', [Validators.required, Validators.min(-15000), Validators.max(15000)]],
    });
  }

  createCoordinateSystem() {
    this.context.lineWidth = 1;
    // creating horizontal line
    this.createLine(150, 0, 150, 300);

    // creating vertical line
    this.createLine(0, 150, 300, 150);

    this.createCircles();

    this.createDiagonalLines();
  }

  clearChart() {
    this.context.clearRect(0, 0, 300, 300);
    this.createCoordinateSystem();
    this.dataService.arrows = [];
  }

  drawArrow(layerX: number, layerY: number): void {
    this.shouldCreateLine
        ? this.createArrow(layerX, layerY)
        : this.drawPoint(layerX, layerY);
    this.shouldCreateLine = !this.shouldCreateLine;
  }

  drawPoint(layerX: number, layerY: number): void {
      this.getArrowDataFromDialog(false).pipe(
          tap(res => this.shouldCreateLine = !!res),
          filter(res => !!res),
      ).subscribe(
          res => {
              this.context.fillRect(layerX, layerY, 3, 3);
              layerX = (layerX - 150) * 100;
              layerY = (150 - layerY) * 100;
              this.dataService.pointWithoutDirection = { depth: res.depth, layerX, layerY };
          }
      );
  }

  createArrow(layerX: number, layerY: number): void {
      this.context.clearRect(this.dataService.pointWithoutDirection.layerX / 100, this.dataService.pointWithoutDirection.layerY / 100, 3, 3);
      this.getArrowDataFromDialog().pipe(
          tap(res => !res && this.clearChart()),
          filter(res => !!res),
      ).subscribe(
          res => {
              this.drawLine(layerX, layerY);
              layerX = (layerX - 150) * 100;
              layerY = (150 - layerY) * 100;
              const targetLocation = {
                  layerX,
                  layerY,
                  depth: res.depth,
              };
              const  newArrow = {
                  velocity: res.velocity,
                  beginLocation: this.dataService.pointWithoutDirection,
                  targetLocation,
              };
              this.dataService.arrows.push(newArrow);
              this.dataService.pointWithoutDirection = null;
          }
      );
  }

  getArrowDataFromDialog(shouldShowVelocity: boolean = true): Observable<ArrowDialogReqInterface> {
      const dialogConfig: MatDialogConfig = {
          width: '640px',
          maxWidth: '90vw',
          data: { shouldShowVelocity },
      };
      const dialogRef = this.dialog.open(ArrowDataDialogComponent, dialogConfig);
      return dialogRef.afterClosed();
  }

  drawLine(layerX: number, layerY: number) {
      const beginLayerX = this.dataService.pointWithoutDirection.layerX / 100 + 150;
      const beginLayerY = 150 - this.dataService.pointWithoutDirection.layerY / 100;
      this.context.beginPath();

      this.context.lineWidth = 3;

      this.context.moveTo(beginLayerX, beginLayerY);
      this.context.lineTo(layerX, layerY);

      const angle = Math.atan2(
          layerY - beginLayerY,
          layerX - beginLayerX,
          );
      const arrowHead = 10;

      this.context.lineTo(
          layerX - arrowHead * Math.cos(angle - Math.PI / 6),
          layerY - arrowHead * Math.sin(angle - Math.PI / 6),
      );

      this.context.moveTo(layerX, layerY);
      this.context.lineTo(
          layerX - arrowHead * Math.cos(angle + Math.PI / 6),
          layerY - arrowHead * Math.sin(angle + Math.PI / 6),
      );

      this.context.stroke();
  }

  addPoint() {
    this.drawArrow(+this.pointForm.get('xPoint').value / 100 + 150, 150 - +this.pointForm.get('yPoint').value / 100);
    this.pointForm.reset();
  }

  createLine(xStart: number, yStart: number, xEnd: number, yEnd: number): void {
    this.context.beginPath();
    this.context.moveTo(xStart, yStart);
    this.context.lineTo(xEnd, yEnd);
    this.context.stroke();
  }

  // creating circles
  createCircles(): void {
    for (let i = 0; i < 10; i++) {
      this.context.beginPath();
      this.context.arc(150, 150, (i + 1) * 15, 0, 2 * Math.PI);
      this.context.stroke();
    }
  }

  // creating diagonal lines
  createDiagonalLines() {
    for (let i = 0; i < 8; i++) {
      const isObtuseAngle = (i === 0 || i === 3 || i === 4 || i === 7);

      const angle = isObtuseAngle ? 60 : 30;
      const radAngle = angle * Math.PI / 180;

      const unknownSide = (Math.tan(radAngle) * 150);
      const isNegativeSide = (i < 2 || (i > 3 && i < 6));

      const xEndPoint = isNegativeSide ? 150 - unknownSide : 150 + unknownSide;
      const yEndPoint = (i < 4) ? 0 : 300;

      this.createLine(150, 150, xEndPoint, yEndPoint);
    }
  }

  mouseMove(event: MouseEvent) {
    this.hoverX = (event.layerX - 150) * 100;
    this.hoverY = (150 - event.layerY) * 100;
  }

  mouseLeave(): void {
    this.hoverX = null;
    this.hoverY = null;
  }

}
