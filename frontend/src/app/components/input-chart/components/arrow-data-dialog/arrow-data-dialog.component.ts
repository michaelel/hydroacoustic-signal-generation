import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-arrow-data-dialog',
  templateUrl: './arrow-data-dialog.component.html',
  styleUrls: ['./arrow-data-dialog.component.sass']
})
export class ArrowDataDialogComponent implements OnInit {

  arrowDataForm: FormGroup;
  shouldShowVelocity: boolean;

  constructor(
      private dialogRef: MatDialogRef<MatDialog>,
      private fb: FormBuilder,
      @Inject(MAT_DIALOG_DATA) data,
  ) {
    this.shouldShowVelocity = data.shouldShowVelocity;
  }

  ngOnInit() {
    this.arrowDataForm = this.fb.group({
      depth: [50, Validators.required],
      velocity: [1, this.shouldShowVelocity && Validators.required],
    });
  }

  close(): void {
    this.dialogRef.close();
  }
  save(): void {
    if (this.arrowDataForm.invalid) { return; }

    this.dialogRef.close(this.arrowDataForm.getRawValue());
  }


}
