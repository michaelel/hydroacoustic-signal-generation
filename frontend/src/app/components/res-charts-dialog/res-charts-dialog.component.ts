import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {ModellingResponseDataInterface} from '../../shared/interfaces/modelling-response-data.interface';
import { BaseChartDirective, Color } from 'ng2-charts';
import { ChartDataSets, ChartOptions } from 'chart.js';

@Component({
  selector: 'app-res-charts',
  templateUrl: './res-charts-dialog.component.html',
  styleUrls: ['./res-charts-dialog.component.sass']
})
export class ResChartsDialogComponent implements OnInit {

  data: ModellingResponseDataInterface[];

  public lineChartData: ChartDataSets[];
  public lineChartLabels;

  tabs = [
    {
      title: 'vx',
      color: 'red',
    },
    {
      title: 'vy',
      color: 'green',
    },
    {
      title: 'vz',
      color: 'blue',
    },
    {
      title: 'p',
      color: 'yellow',
    },
  ];

  public lineChartType = 'line';

  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [{}]
    },
  };

  @ViewChild(BaseChartDirective) chart: BaseChartDirective;



  constructor(
      private dialogRef: MatDialogRef<MatDialog>,
      @Inject(MAT_DIALOG_DATA) data,
  ) {
    this.data = data;
    this.draw(this.tabs[0]);
    this.lineChartLabels = this.data.map(item => item.time);
  }

  ngOnInit() {
  }

  draw(tab: { title: string, color: string }): void {
    this.lineChartData = [{ data: this.data.map(item => item[tab.title]), label: tab.title }];
  }
}
