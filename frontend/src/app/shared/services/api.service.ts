import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CatInterface } from '../interfaces/cat.interface';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';
import {ModelingInterface} from '../interfaces/modeling.interface';
import {ModellingResponseDataInterface} from "../interfaces/modelling-response-data.interface";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getAllCats(): Observable<CatInterface[]> {
    return this.http.get<CatInterface[]>('http://localhost:8000/api/cats').pipe(
      pluck('cats'),
    );
  }

  // Метод для посилання запиту на серверну частину для початку моделювання
  modeling(payload: ModelingInterface): Observable<ModellingResponseDataInterface[]> {
    return this.http.post<ModellingResponseDataInterface[]>('http://localhost:8000/api/modeling', { params: payload, observe: 'response' }).pipe(
        pluck('result'),
    );
  }
}
