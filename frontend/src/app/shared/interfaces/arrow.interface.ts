import {CoordinateInterface} from './coordinate.interface';

export interface ArrowInterface {
    beginLocation: CoordinateInterface;
    targetLocation: CoordinateInterface;
    velocity: number | string;
}
