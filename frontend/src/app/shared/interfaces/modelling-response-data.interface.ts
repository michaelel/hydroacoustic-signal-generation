export interface ModellingResponseDataInterface {
    time: string | number;
    vx: string | number;
    vy: string | number;
    vz: string | number;
    p: string | number;
}
