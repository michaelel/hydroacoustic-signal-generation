export interface SignalFormInterface {
    timeStart: number | string;
    timeEnd: number | string;
    seaDepth: number | string;
    gasDepth: number | string;
    amplitude: number | string;
    frequency: number | string;
    soundSpeed: number | string;
}
