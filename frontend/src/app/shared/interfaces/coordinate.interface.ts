export interface CoordinateInterface {
    layerX: number;
    layerY: number;
    depth: number;
}
