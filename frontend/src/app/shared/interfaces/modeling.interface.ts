import {ArrowInterface} from './arrow.interface';
import {SignalFormInterface} from './signal-form.interface';

export interface ModelingInterface extends SignalFormInterface {
    arrows: ArrowInterface[];
}
