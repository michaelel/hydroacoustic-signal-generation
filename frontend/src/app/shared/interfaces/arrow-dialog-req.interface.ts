export interface ArrowDialogReqInterface {
    depth: number;
    velocity?: number;
}
