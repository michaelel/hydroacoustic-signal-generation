import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputChartComponent } from './components/input-chart/input-chart.component';
import { InputFormComponent } from './components/input-form/input-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from "./shared/modules/material/material.module";
import {ChartsModule} from "ng2-charts";
import {MomentDateModule} from "@angular/material-moment-adapter";
import { InputAppearenceComponent } from './components/input-appearence/input-appearence.component';
import { ReactiveFormsModule } from "@angular/forms";
import { ArrowDataDialogComponent } from './components/input-chart/components/arrow-data-dialog/arrow-data-dialog.component';
import { ApiService } from "./shared/services/api.service";
import { HttpClientModule } from "@angular/common/http";
import { ResChartsDialogComponent } from './components/res-charts-dialog/res-charts-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    InputChartComponent,
    InputFormComponent,
    InputAppearenceComponent,
    ArrowDataDialogComponent,
    ResChartsDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    ChartsModule,
    MomentDateModule,
    HttpClientModule,
  ],
  providers: [
    ApiService,
  ],
  entryComponents: [
    ArrowDataDialogComponent,
    ResChartsDialogComponent,
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
