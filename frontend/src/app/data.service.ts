import { Injectable } from '@angular/core';
import {ArrowInterface} from './shared/interfaces/arrow.interface';
import {SignalFormInterface} from './shared/interfaces/signal-form.interface';
import {ApiService} from './shared/services/api.service';
import {CoordinateInterface} from './shared/interfaces/coordinate.interface';
import {Subject} from 'rxjs';
import {ModellingResponseDataInterface} from './shared/interfaces/modelling-response-data.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  arrows: ArrowInterface[] = [];
  pointWithoutDirection: CoordinateInterface;
  inProgress$: Subject<boolean> = new Subject();
  dataToBuildCharts: ModellingResponseDataInterface[];

  constructor (
      private apiService: ApiService,
  ) {
    this.inProgress$.next(false);
  }

  modeling(signalFormData: SignalFormInterface): void {
    // Запис в змінну для запиту з правильною структурою
    const req = {
      ...signalFormData,
      arrows: this.arrows,
      pointWithoutDirection: this.pointWithoutDirection,
    };
    this.inProgress$.next(true);
    // Звернення до API сервісу для запиту на початок моделювання
    this.apiService.modeling(req).subscribe(
        // Успішний сценарій моделювання
        res => {
            this.inProgress$.next(false);
            const shouldDownload = confirm('Файл успешно сгенерирован! Скачать файл?');
            if (shouldDownload) {
                // Початок завантаження файлу з сервера
                const link = document.getElementById('download-link');
                link.click();
            }
            // Збереження даних про сигнал
            this.dataToBuildCharts = res;
        },
        // Обробка помилки
        e => alert(e.message || e),
    );
  }
}
