const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const api = require('./modules/api-module.js');


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(8000, () => {
    console.log('Server started!');
});

api.listenHttpRequests(app);




