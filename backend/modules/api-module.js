const modelling = require('./modelling-module');

function listenHttpRequests(app) {
    app.route('/api/cats').get((req, res) => {
        res.send({
            cats: [{ name: 'lilly' }, { name: 'lucy' }]
        });
    });
    // Код на запит з клієнтської сторони на моделювання
    app.post('/api/modeling', (req, res) => {
        res.send({result: modelling.runModelling(req.body.params)});
    });

    // Код на запит з клієнтської сторони на завантаження
    app.get('/api/download', (req, res) => {
        res.download('signal.dat');
    });

}

exports.listenHttpRequests = listenHttpRequests;
