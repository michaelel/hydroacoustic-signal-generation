const vectorModule = require('./vector-module');

function getTrajectory(points) {
    return { points, length: getLength(points) };
}

function getLength(points) {
    let res = 0;
    points.forEach((point, i) => {
        if (!points[i +  1]) { return; }
        res += vectorModule.getDistance(point, points[i + 1])
    });

    return res;
}

exports.getTrajectory = getTrajectory;
