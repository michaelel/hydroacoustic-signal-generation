const fs = require('fs');
const BinaryWriter = require('./../libs/BinaryWriter-node.js/BinaryWriter.js');


const bw = new BinaryWriter();

function save(res) {
    const response = writeBinary(res);
    fs.writeFile('./signal.dat', response, e => {
        console.log(e || 'File successfully loaded.');
    });
    return res;
}

function writeBinary(res) {
    res.forEach(el => {
        bw.writeInt8(el.p);
        bw.writeInt8(el.vx);
        bw.writeInt8(el.vy);
        bw.writeInt8(el.vz);
    });
    return bw.toBuffer();
}

exports.save = save;
