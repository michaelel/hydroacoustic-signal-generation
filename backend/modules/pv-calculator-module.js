const vectorModule = require('./vector-module');
const rayTracer = require('./ray-tracer');

const r0 = 10000;
let objects;
let seaDepth;
let soundSpeed;
let reflections;
let amplitude;
let gasPosition;

function init(payload) {
        objects = payload.arrows;
        seaDepth = payload.seaDepth;
        soundSpeed = payload.soundSpeed;
        reflections = payload.reflections;
        amplitude = payload.amplitude;
        gasPosition = vectorModule.getVector(0, 0, payload.gasDepth);
}

function getPV(currentTime) {
    let res = {};

    objects.forEach(obj => {
        // Знаходження всіх променів відбиття в даний час
        const rays = rayTracer.getRays(getPosition(obj, currentTime), gasPosition, seaDepth);
        obj.frequencies.forEach(freq => {
            rays.forEach(ray =>{
                // Дистанція від променя до вектора
                const r = getDistanceVector(ray);
                const dist = vectorModule.getLength(r);
                // Амплітуда
                const a = getAmplitude(freq.amplitude, dist);
                // Довжина хвилі
                const waveLen = getWaveLength(freq.frequency);
                // Вектор хвилі
                const k = getWaveVector(waveLen, r);
                // Зміна фази
                const phaseChange = getPhaseChange(freq.frequency, dist);
                // Швидкість
                const v = getVelocity(freq.frequency, a, k, r, currentTime, phaseChange);
                // Тиск
                const p = getPressure(freq.frequency, a, k, r, currentTime, phaseChange);
                res.velocity = res.velocity ? vectorModule.add(res.velocity, v) : v;
                res.pressure = res.pressure ? vectorModule.add(res.pressure, p) : p;
            });
        });
    });
    return res;
}

function getPosition(obj, time) {
    const beginLoc = vectorModule.getVector(obj.beginLocation.layerX, obj.beginLocation.layerY, obj.beginLocation.depth);
    const targetLoc= vectorModule.getVector(obj.targetLocation.layerX, obj.targetLocation.layerY, obj.targetLocation.depth);
    return vectorModule.add(beginLoc, vectorModule.multiple(vectorModule.multiple(vectorModule.unitary(vectorModule.subtract(targetLoc, beginLoc)), +obj.velocity), +time));
}

function getAmplitude(a0, dist) {
    return dist < Number.EPSILON
        ? a0
        : a0 * Math.exp(-amplitude * dist);
}

function getDistanceVector(ray) {
    const rayLen = ray.length;
    const n = ray.points.length;
    const dir = vectorModule.unitary(vectorModule.subtract(ray.points[n - 1], ray.points[n - 2]));
    return vectorModule.multiple(dir, rayLen);
}

function getWaveLength(waveFrequency) {
    return soundSpeed * waveFrequency;
}

function getWaveVector(waveLen, r) {
    const k = getWaveNumber(waveLen);
    return vectorModule.multiple(vectorModule.unitary(r), k);
}

function getWaveNumber(waveLen) {
    return 2 * Math.PI / waveLen;
}

function getPhaseChange(waveFrequency, distance) {
    return 2 * Math.PI * distance * waveFrequency / soundSpeed;
}

function getVelocity(frequency, a, k, r, t, phaseShift) {
    const w = frequency * 2 * Math.PI;
    const amp = w * a;
    const phi = vectorModule.multiple(k, r) - w * t + phaseShift + Math.PI;
    return getRotatedVectorWithLength(amp, phi, r);
}

function getPressure(frequency, a, k, r, t, phaseShift) {
    const w = frequency * 2 * Math.PI;
    const amp = r0 * soundSpeed * soundSpeed * vectorModule.getLength(k) * a;
    const phi = vectorModule.multiple(k, r) - w * t + phaseShift + Math.PI / 2;
    return getRotatedVectorWithLength(amp, phi, r);
}

function getRotatedVectorWithLength(max, phase, R)
{
    const xy = vectorModule.getVector(R.x, R.y, 0);
    const planeVector = vectorModule.getVector(vectorModule.getLength(xy), R.z, 0);
    const rotPlaneVector = vectorModule.rotateByZ(planeVector, phase);
    const resVector = vectorModule.multiple(vectorModule.unitary(xy), rotPlaneVector.x);
    resVector.z = rotPlaneVector.y;
    return vectorModule.multiple(vectorModule.unitary(resVector), max);
}


exports.init = init;
exports.getPV = getPV;
